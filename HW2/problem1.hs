import System.Random
import Statistics.Sample as S
import qualified Data.Vector as V
import Control.Monad.IO.Class
import Data.List as L
import System.Environment

flipCoin n = do
    gen <- newStdGen
    let xs = randoms gen :: [Double]

    return $! (sum $ map (\x ->
                             if x < 0.5
                             then 0.0
                             else 1.0) $
              take n xs) / (fromIntegral n :: Double)

flipCoins m n = map (\_ -> flipCoin n) $ take m $ repeat 0

runStats m n = do
    s <- sequence $ flipCoins m n
    let cc = V.fromList s
    
    gen <- newStdGen
    let r = randoms gen :: [Double]
    let i = s!!(floor $ ((fromIntegral $ length s :: Double) - 1) * (take 1 r)!!0)
    
    let x = V.fromList [s!!0,
                        i,
                        V.minimum cc]

    return x
    
run n = do
    s <- sequence $! map (\_ -> runStats 1000 10) $ take n $ repeat 0

    let firsts = S.mean $ V.fromList $ map (\r -> r V.! 0) s
    let rands = S.mean $ V.fromList $ map (\r -> r V.! 1) s
    let mins = S.mean $ V.fromList $ map (\r -> r V.! 2) s
    
    return [firsts, rands, mins]

    
main = do
    args <- getArgs
    let count = read $ args!!0 :: Int
    run count >>= print
