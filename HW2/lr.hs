import Statistics.LinearRegression
import qualified Data.Vector.Unboxed as U

test :: Int -> Int -> IO ()
test k n = do
    let a = fromIntegral (k*n + 1) :: Double
    let b = fromIntegral ((k+1)*n) :: Double
    let xs = U.fromList [a..b]
    let ys = U.map (\x -> x*100 + 2000) xs
    -- thus 100 and 2000 are the alpha and beta we want
    putStrLn "linearRegression:"
    print $ linearRegression xs ys
