import Control.Monad.Random
import Control.Monad

data Point = Point Double Double
             deriving Show
             
coord :: (RandomGen g) => Rand g Point
coord = do
    x <- getRandom
    y <- getRandom

    return $ Point x y

line :: Point -> Point -> (Double -> Double)
line (Point x1 y1) (Point x2 y2) = (\x -> ((y2 - y1) / (x2 - x1)) * (x - x1) + y1)

samples :: (RandomGen g) => (Double -> Double) -> Int -> Rand g [Point]
samples l n = do
    s <- sequence (replicate n getRandom)
    return $ map (\(x, y) -> Point x y) $ zip s $ map l s

initvec :: Point -> (Double, Double)
initvec (Point x y) = (1, y)

main = do
    p0 <- evalRandIO coord
    p1 <- evalRandIO coord

    let l = line p0 p1

    s <- evalRandIO $ samples l 10
        
    let ivs = map initvec s

    print ivs