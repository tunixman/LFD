-- Howemork 5 Problem 2
--
-- Use a line z2 = z1 - 1 as the separator, calculate w~

import Algebra.Linreg
import Control.Monad.Random
import Control.Monad
import Data.Packed.Matrix
import Text.Printf

data Point = Point Double Double
             deriving Show

coord :: (RandomGen g) => Rand g Point
coord = do
    x <- getRandom
    y <- getRandom

    return $ Point x y

line :: Double -> Double
line z1 = z1 - 1

lineRel :: (Double -> Double) -> Point -> Double
lineRel l (Point z1 z2) = z2 - signum (l z1)

coords :: (RandomGen g) => Int -> Rand g [Point]
coords n = sequence (replicate n coord)

samples :: (Double -> Double) -> [Point] -> [Sample]
samples l ps = map (\(Point x1 x2) ->
                        (Sample (lineRel l (Point x1 x2) :: Double) x1 x2)) ps

experiment :: Int -> IO (Matrix Double)
experiment n = do
  p0 <- evalRandIO coord
  p1 <- evalRandIO coord

  s <- evalRandIO $ coords n

  let svec = sampleVec $ samples line s
  let xs = xVec svec
  let ys = yVec svec
      
  let w = wVec xs ys

  return $ wVec xs ys

main = do
    w <- experiment 100000

    print w

    