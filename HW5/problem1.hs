import System.Environment
import Options.Applicative
import Text.Printf

data Params = Params {
                     s :: Double,
                     d :: Integer,
                     n :: Integer
                     }

params :: Parser Params
params = Params
         <$> option (long "sigma"
                     <> metavar "VARIANCE(D)"
                     <> help "The variance of the distribution.")
         <*> option (long "dimensions"
                     <> metavar "DIMENSIONS(I)"
                     <> help "The dimensionality of the independent vector.")
         <*> option (long "samples"
                     <> metavar "SAMPLES(I)"
                     <> help "The number of samples to consider.")
         
e_in :: Double -> Integer -> Integer -> Double
e_in s d n = s ** 2 * ( 1 - (((fromIntegral d :: Double) + 1) / (fromIntegral n:: Double)))

run :: Params -> IO ()
run (Params s d n) = printf "%8.4f\n" $ e_in s d n

main = do
    execParser opts >>= run
      where opts = info (helper <*> params)
              (fullDesc <>
               progDesc "Calculate Ein from variance, dimensions, and samples." <>
               header "Homework 5 Problem 1")
